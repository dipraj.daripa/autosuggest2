
// #include <bits/stdc++.h>
// #include <unordered_map>
// using namespace std;

// #define MAX_TREE_HT 256
// // to map each character its huffman value
// map<char, string> codes;

// // to store the frequency of character of the input data
// map<char, int> freq;

// // A Huffman tree node
// struct MinHeapNode
// {
// 	char data;			 // One of the input characters
// 	int freq;			 // Frequency of the character
// 	MinHeapNode *left, *right; // Left and right child

// 	MinHeapNode(char data, int freq)
// 	{
// 		left = right = NULL;
// 		this->data = data;
// 		this->freq = freq;
// 	}
// };

// // utility function for the priority queue
// struct compare
// {
// 	bool operator()(MinHeapNode* l, MinHeapNode* r)
// 	{
// 		return (l->freq > r->freq);
// 	}
// };

// // utility function to print characters along with
// // there huffman value
// void printCodes(struct MinHeapNode* root, string str)
// {
// 	if (!root)
// 		return;
// 	if (root->data != '$')
// 		cout << root->data << ": " << str << "\n";
// 	printCodes(root->left, str + "0");
// 	printCodes(root->right, str + "1");
// }

// // utility function to store characters along with
// // there huffman value in a hash table, here we
// // have C++ STL map
// void storeCodes(struct MinHeapNode* root, string str)
// {
// 	if (root==NULL)
// 		return;
// 	if (root->data != '$')
// 		codes[root->data]=str;
// 	storeCodes(root->left, str + "0");
// 	storeCodes(root->right, str + "1");
// }

// // STL priority queue to store heap tree, with respect
// // to their heap root node value
// priority_queue<MinHeapNode*, vector<MinHeapNode*>, compare> minHeap;

// // function to build the Huffman tree and store it
// // in minHeap
// void HuffmanCodes(int size)
// {
// 	struct MinHeapNode *left, *right, *top;
// 	for (map<char, int>::iterator v=freq.begin(); v!=freq.end(); v++)
// 		minHeap.push(new MinHeapNode(v->first, v->second));
// 	while (minHeap.size() != 1)
// 	{
// 		left = minHeap.top();
// 		minHeap.pop();
// 		right = minHeap.top();
// 		minHeap.pop();
// 		top = new MinHeapNode('$', left->freq + right->freq);
// 		top->left = left;
// 		top->right = right;
// 		minHeap.push(top);
// 	}
// 	storeCodes(minHeap.top(), "");
// }

// // utility function to store map each character with its
// // frequency in input string
// void calcFreq(string str, int n)
// {
// 	for (int i=0; i<str.size(); i++)
// 		freq[str[i]]++;
// }

// // function iterates through the encoded string s
// // if s[i]=='1' then move to node->right
// // if s[i]=='0' then move to node->left
// // if leaf node append the node->data to our output string
// string decode_file(struct MinHeapNode* root, string s)
// {
// 	string ans = "";
// 	struct MinHeapNode* curr = root;
// 	for (int i=0;i<s.size();i++)
// 	{
// 		if (s[i] == '0')
// 		curr = curr->left;
// 		else
// 		curr = curr->right;

// 		// reached leaf node
// 		if (curr->left==NULL and curr->right==NULL)
// 		{
// 			ans += curr->data;
// 			curr = root;
// 		}
// 	}
// 	// cout<<ans<<endl;
// 	return ans;
// }










// struct Trie {

// 	bool isEndOfWord;

// 	/* nodes store a map to child node */
// 	map<char, Trie*> mp;
// };

// string readFileIntoString(const string& path) {
//     ifstream input_file(path);
//     if (!input_file.is_open()) {
//         cerr << "Could not open the file - '"
//              << path << "'" << endl;
//         exit(EXIT_FAILURE);
//     }
//     return string((std::istreambuf_iterator<char>(input_file)), std::istreambuf_iterator<char>());
// }

// Trie* getNewTrieNode()
// {
// 	Trie* node = new Trie;
// 	node->isEndOfWord = false;
// 	return node;
// }


// void insert(Trie*& root, const string& str)
// {
// 	if (root == nullptr)
// 		root = getNewTrieNode();

// 	Trie* temp = root;
// 	for (int i = 0; i < str.length(); i++) {
// 		char x = str[i];

// 		/* make a new node if there is no path */
// 		if (temp->mp.find(x) == temp->mp.end())
// 			temp->mp[x] = getNewTrieNode();

// 		temp = temp->mp[x];
// 	}

// 	temp->isEndOfWord = true;
// }

// string serialize(Trie* root) {
//         string result;
//         if(!root)
//             return result;
//         result += "<";
//         for(char c = 'a'; c <= 'z'; c++){
//             if(root->mp.find(c) != root->mp.end()){
//                 result += string(1, c);
//                 result += serialize(root->mp[c]);
//             }
//         }
//         result += ">";
//         return result;
// }

// Trie* deserialize(string data) {
//         if(data.empty())
//             return NULL;
//         Trie* root = new Trie();
//         Trie* current = root;
//         stack<Trie*> stk;
//         for(char c: data){
//             switch(c){
//                 case '<':
//                     stk.push(current);
//                     break;
               
//                 case '>':
//                     stk.pop();
//                     current->isEndOfWord = true;
//                     break;
//                 default:
//                     current = new Trie();
//                     stk.top()->mp[c] = current;
//             }
//         }
//         return root;
// }

// // /*function to search in trie*/
// bool search(Trie* root, const string& str)
// {
// 	/*return false if Trie is empty*/
// 	if (root == nullptr)
// 		return false;

// 	Trie* temp = root;
// 	for (int i = 0; i < str.length(); i++) {

// 		/* go to next node*/
// 		temp = temp->mp[str[i]];

// 		if (temp == nullptr)
// 			return false;
// 	}

// 	return temp->isEndOfWord;
// }
// void serchWord(Trie* currNode, vector<string> &ans, string word){
    
//     if(currNode == nullptr){
//             return;
//     }
//     if(currNode->isEndOfWord){
        
//         ans.push_back(word);
        
//     }
//     map<char, Trie*> mp_1 = currNode->mp;
    
//     for(auto it: mp_1){
//         string temp = word+it.first;
//         //cout << temp << endl;
//         serchWord(mp_1[it.first], ans, temp);
//     }
// }

// vector<string> advSerach(Trie* root, string str){
//     vector<string> ans;
    
//     Trie* currNode = root;
    
//     for(int i=0;i<str.length();i++) {
//         //cout << currNode -> mp[str[i]]<< endl;
        
//         currNode  = currNode -> mp[str[i]];
//         if(currNode == nullptr){
//             return ans;
//         }
//     }
    
//     serchWord(currNode, ans, str);
    
//     return ans;
// }

// /*Driver function*/
// int main()
// {
// 	Trie* root = nullptr;


// 	// insert(root, "hello");
	
// 	// insert(root,"help");
// 	// insert(root,"helium");

//     	string fname = "EnglishDictionary.csv";

   
// 	vector<vector<string>> content;
// 	vector<string> row;
// 	string line, word;
 
// 	fstream file (fname, ios::in);
// 	if(file.is_open())
// 	{
// 		while(getline(file, line))
// 		{
// 			row.clear();
 
// 			stringstream str(line);
 
// 			while(getline(str, word, ','))
// 				row.push_back(word);
// 			content.push_back(row);
// 		}
// 	}
// 	else{
// 		cout<<"Could not open the file\n";
//     }

    

//     map<string,int> dict;
 
// 	for(int i=0;i<content.size();i++)
// 	{
// 		dict[content[i][0]] = stoi(content[i][1]) + stoi(content[i][2]);
		
		
// 	}

//     for(auto it: dict){
//          insert(root, it.first);
//     }

// 	string s = serialize(root);

//     // string encodedString, decodedString;
// 	// calcFreq(s, s.length());
// 	// HuffmanCodes(s.length());

//     // for (auto i: s)
// 	// 	encodedString+=codes[i];

//     char *buf;
//     int sz = s.size();

// 	std::ofstream out("serial trie.bin" , ios::out | ios::binary);
//     if(!out){
//         cout <<"can't open" << endl;
//         exit(1);
//     }
//     out.write(reinterpret_cast<char *> (&sz), sizeof(sz));
//     out.write(s.data(), sz);
//     out.flush();
//     out.close();
	
// 	ifstream filename("serial trie.bin",ios::in | ios::binary);
//     if(!out){
//         cout <<"can't open" << endl;
//         exit(1);
//     }
//     filename.read(reinterpret_cast<char *> (&sz), sizeof(sz));
//     buf = new char[sz];
//     filename.read(buf, sz);

//     string file_contents = "";
//     file_contents.append(buf,sz);

//     // file_contents = decode_file(minHeap.top(), file_contents);

//     // cout << decodedString << endl;
    
    
// 	Trie* root2 = nullptr;
	
// 	root2 = deserialize(file_contents);
// 	//cout << search(root2,"helium");
	
// 	vector<string> ans1 = advSerach(root2, "abo");
// 	for(auto it: ans1){
// 	    cout << it <<" ";
// 	}

// 	return 0;
// }



#include <bits/stdc++.h>
#include <unordered_map>
using namespace std;


struct Trie {

	bool isEndOfWord;

	/* nodes store a map to child node */
	map<char, Trie*> mp;
};

string readFileIntoString(const string& path) {
    ifstream input_file(path);
    if (!input_file.is_open()) {
        cerr << "Could not open the file - '"
             << path << "'" << endl;
        exit(EXIT_FAILURE);
    }
    return string((std::istreambuf_iterator<char>(input_file)), std::istreambuf_iterator<char>());
}

Trie* getNewTrieNode()
{
	Trie* node = new Trie;
	node->isEndOfWord = false;
	return node;
}


void insert(Trie*& root, const string& str)
{
	if (root == nullptr)
		root = getNewTrieNode();

	Trie* temp = root;
	for (int i = 0; i < str.length(); i++) {
		char x = str[i];

		/* make a new node if there is no path */
		if (temp->mp.find(x) == temp->mp.end())
			temp->mp[x] = getNewTrieNode();

		temp = temp->mp[x];
	}

	temp->isEndOfWord = true;
}

string serialize(Trie* root) {
        string result;
        if(!root)
            return result;
        result += "<";
        for(char c = 'a'; c <= 'z'; c++){
            if(root->mp.find(c) != root->mp.end()){
                result += string(1, c);
                result += serialize(root->mp[c]);
            }
        }
        result += ">";
        return result;
}

Trie* deserialize(string data) {
        if(data.empty())
            return NULL;
        Trie* root = new Trie();
        Trie* current = root;
        stack<Trie*> stk;
        for(char c: data){
            switch(c){
                case '<':
                    stk.push(current);
                    break;
               
                case '>':
                    stk.pop();
                    current->isEndOfWord = true;
                    break;
                default:
                    current = new Trie();
                    stk.top()->mp[c] = current;
            }
        }
        return root;
}

// /*function to search in trie*/
bool search(Trie* root, const string& str)
{
	/*return false if Trie is empty*/
	if (root == nullptr)
		return false;

	Trie* temp = root;
	for (int i = 0; i < str.length(); i++) {

		/* go to next node*/
		temp = temp->mp[str[i]];

		if (temp == nullptr)
			return false;
	}

	return temp->isEndOfWord;
}
void serchWord(Trie* currNode, vector<string> &ans, string word){
    
    if(currNode == nullptr){
            return;
    }
    if(currNode->isEndOfWord){
        
        ans.push_back(word);
        
    }
    map<char, Trie*> mp_1 = currNode->mp;
    
    for(auto it: mp_1){
        string temp = word+it.first;
        //cout << temp << endl;
        serchWord(mp_1[it.first], ans, temp);
    }
}

vector<string> advSerach(Trie* root, string str){
    vector<string> ans;
    
    Trie* currNode = root;
    
    for(int i=0;i<str.length();i++) {
        //cout << currNode -> mp[str[i]]<< endl;
        
        currNode  = currNode -> mp[str[i]];
        if(currNode == nullptr){
            return ans;
        }
    }
    
    serchWord(currNode, ans, str);
    
    return ans;
}

/*Driver function*/
int main()
{
	Trie* root = nullptr;


	// insert(root, "hello");
	
	// insert(root,"help");
	// insert(root,"helium");

    	string fname = "EnglishDictionary.csv";

   
	vector<vector<string>> content;
	vector<string> row;
	string line, word;
 
	fstream file (fname, ios::in);
	if(file.is_open())
	{
		while(getline(file, line))
		{
			row.clear();
 
			stringstream str(line);
 
			while(getline(str, word, ','))
				row.push_back(word);
			content.push_back(row);
		}
	}
	else{
		cout<<"Could not open the file\n";
    }

    

    map<string,int> dict;
 
	for(int i=0;i<content.size();i++)
	{
		dict[content[i][0]] = stoi(content[i][1]) + stoi(content[i][2]);
		
		
	}

    for(auto it: dict){
         insert(root, it.first);
    }

	string s = serialize(root);

    
    vector<int> fnum;
    for(auto it: s){
        fnum.push_back(int(it));
    }
    
 	int sz = fnum.size();
  	int i;

  	ofstream out("serial.cgt", ios::out | ios::binary);
  	if(!out) {
    	cout << "Cannot open file.";
    	return 1;
  	}

  	out.write((char *) &fnum, sizeof fnum);

  	out.close();

  	fnum.clear();

  	ifstream in("serial.cgt", ios::in | ios::binary);
  	in.read((char *) &fnum, sizeof fnum);

  	// see how many bytes have been read
  	cout << in.gcount() << " bytes read\n";
  
  	string file_contents = "";

  	for(i=0; i<sz; i++){ // show values read from file
     	file_contents +=char(fnum[i]);
  	}
  
  

  	in.close();
    
    
	Trie* root2 = nullptr;
	
	root2 = deserialize(file_contents);
	//cout << search(root2,"helium");
	
	vector<string> ans1 = advSerach(root2, "abo");
	for(auto it: ans1){
	    cout << it <<" ";
	}

	return 0;
}